<?php
/*
 * Metadata statement for the target of the manpage links.
 */
$meta['mantarget'] = array('multichoice', '_choices' => array('NetBSD', 'OpenBSD', 'FreeBSD', 'DragonFlyBSD'));
//Setup VIM: ex: et ts=2 enc=utf-8 :
